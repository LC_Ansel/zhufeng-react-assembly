今天需要做个表格，但是做表格前需要完成分页。

## 分页组件

做这个组件，需要2个状态，一个是当前值，一个是当前显示页码的映射表。

到时候，我们还会向外暴露分页最大显示长度、每页显示多少条

这个每页显示多少条是在分页内部进行计算。比如我向组件传递总共100条，每页20条，那么显示5页，以此类推。因为一般情况都是直接把数据length传给分页，而不在外面进行计算。

我们可以先设总共1000条，每页显示10条，分页最大显示长度为5，算出number为页数，然后判断页数是不是大于最大分页数，如果大于，那么就用最大分页数来初始化map，如果小于，那么就用页数初始化map。

```tsx
export function Pagination(props: PaginationProps) {
	const pageSize = 10;
	const defaultCurrent = 1;
	const barMaxSize = 5;
	const total = 1000;
	const [current, setCurrent] = useState(defaultCurrent);
	const [state, setState] = useState<Array<number>>([]);
	const totalPage = useMemo(() => {
		let number = Math.ceil(total / pageSize);
		if (number > barMaxSize) {
			let statetmp = new Array(barMaxSize).fill(1).map((x, y) => y + 1);
			setState(statetmp);
		} else {
			let statetmp = new Array(number).fill(1).map((x, y) => y + 1);
			setState(statetmp);
		}
		return number;
	}, [pageSize, total]);
	console.log(totalPage);
	return (
		<ul style={{ display: "flex" }}>
			<li>
				<Button appearance="primaryOutline">
					<Icon icon="arrowleft" color={color.primary}></Icon>
				</Button>
			</li>
			{state.map((x, i) => {
				return (
					<li key={i}>
						<Button
							appearance={
								current === x ? "primary" : "primaryOutline"
							}
							onClick={() => {
								setCurrent(x);
							}}
						>
							{x}
						</Button>
					</li>
				);
			})}
			<li>
				<Button appearance="primaryOutline">
					<Icon icon="arrowright" color={color.primary}></Icon>
				</Button>
			</li>
		</ul>
	);
}
```

由于样式比较丑，需要修改下。

使用styledComponent替换最外层容器：

```tsx
const PageUl = styled.ul`
	display: flex;
	justify-content: center;
	align-items: center;

	& > li {
		list-style: none;
	}
	& button {
		border-radius: 6px;
		padding: 10px 8px;
	}
	& span {
		line-height: 13.6px;
		height: 13.6px;
		min-width: 18px;
	}
	& svg {
		height: 13.6px;
		width: 13.6px;
		vertical-align: bottom;
	}
`;
```

这样就好看多了，下面需要动态计算映射的分页，计算同时还需要保证选中的数值只要不是在边界就会出现在最中间，使用一个方法：

```typescript
function calculateMove(current: number, state: number[], totalPage: number): number[] | null {
	let mid = Math.floor(state.length / 2);
	let arr;
	let minus = current - state[mid];
	if (minus === 0) {
		arr = null;
	} else if (minus > 0) {
		let tmp = state[state.length - 1];
		if (tmp + minus < totalPage) {
			arr = state.map((v) => v + minus);
		} else {
			if (tmp === totalPage) {
				arr = null;
			} else {
				arr = state.map((v) => v + totalPage - tmp);
			}
		}
	} else {
		//负数
		if (state[0] + minus > 1) {
			arr = state.map((v) => v + minus);
		} else {
			//边缘，看最大能减几
			if (state[0] === 1) {
				arr = null;
			} else {
				arr = state.map((v) => v - state[0] + 1);
			}
		}
	}
	return arr;
}
```

这里返回null的话代表不更新，只有arr存在才会更新映射的map。

然后修改下初始化内容，因为要配合默认值，所以需要将默认值传入再计算一次map：

```typescript
	const totalPage = useMemo(() => {
		let number = Math.ceil(total / pageSize);
		if (number > barMaxSize) {
			let statetmp = new Array(barMaxSize).fill(1).map((_x, y) => y + 1);
			setState(statetmp);
			let arr = calculateMove(defaultCurrent, statetmp, number);
			if (arr) {
				setState(arr);
			}
		} else {
			let statetmp = new Array(number).fill(1).map((_x, y) => y + 1);
			setState(statetmp);
			let arr = calculateMove(defaultCurrent, statetmp, number);
			if (arr) {
				setState(arr);
			}
		}
		return number;
	}, [pageSize, total]);
```

这样可以试验下，把defaultCurrent改成11，是不是初始值自动跑11去了。

下面修改onclick，只要在onclick下面加2句就ok:

```
	onClick={() => {
								setCurrent(x);
								let arr = calculateMove(x, state, totalPage);
								if (arr) {
									setState(arr);
								}
							}}
```

可以试验下中间部分基本完成。

下面制作左右2个按钮的状态，左右2个按钮会有边界问题，需要配置disabled：

```tsx
		<Button
					appearance="primaryOutline"
					disabled={current === 1 ? true : false}
					onClick={() => {
						if (state.length > 0) {
							if (state[0] > 1) {
								let statetmp = state.map((x) => x - 1);
								setState(statetmp);
								setCurrent(current - 1);
								let arr = calculateMove(
									current - 1,
									statetmp,
									totalPage
								);
								if (arr) {
									setState(arr);
								}
							} else if (current !== state[0]) {
								setCurrent(current - 1);
								let arr = calculateMove(
									current - 1,
									state,
									totalPage
								);
								if (arr) {
									setState(arr);
								}
							}
						}
					}}
				>
```

右边那个同样：

```tsx
	<Button
					appearance="primaryOutline"
					disabled={current === totalPage ? true : false}
					onClick={() => {
						if (state.length > 0) {
							if (state[barMaxSize! - 1] < totalPage) {
								let statetmp = state.map((x) => x + 1);
								setState(statetmp);
								setCurrent(current + 1);
								let arr = calculateMove(
									current + 1,
									statetmp,
									totalPage
								);
								if (arr) {
									setState(arr);
								}
							} else {
								if (current !== totalPage) {
									setCurrent(current + 1);
									let arr = calculateMove(
										current + 1,
										state,
										totalPage
									);
									if (arr) {
										setState(arr);
									}
								}
							}
						}
					}}
				>
```

这样就完成了，剩下暴露接口。

把开始那些固定值提取到外面，再加个回调：

```
	useEffect(()=>{
		if(callback)callback(current)
	},[callback, current])
```

```
export type PaginationProps = {
	/** 每页显示多少条*/
	pageSize?: number;
	/** 默认显示第几页 */
	defaultCurrent?: number;
	/** 总共条数*/
	total: number;
	/** 分页条目最大显示长度 */
	barMaxSize?: number;
	/** 回调页数 */
	callback?: (v: number) => void;
	/** 外层style*/
	style?:CSSProperties;
	/**外层类名 */
	classnames?:string;
};
```

```
Pagination.defaultProps = {
	pageSize: 10,
	defaultCurrent: 11,
	barMaxSize: 5,
	total: 1000,
};
```

下面制作knobStory：

```
export const knobsPagination = () => (
	<Pagination
		defaultCurrent={number("defualtCurrent", 1)}
		total={number("total", 100)}
		barMaxSize={number("barMaxSize", 5)}
		pageSize={number("pageSize", 5)}
		callback={action("callback")}
	></Pagination>
);
```

## 表格

前面做分页就是为了数据多时进行分页的，这玩意不做无限滚动效果，有兴趣同学可以做做玩玩。

做table前，先把渲染数据弄上，这个dataIndex是data里用来对应表头的英文的。

```tsx
const columns = [
	{
		title: "Name",
		dataIndex: "name",
	},
	{
		title: "Chinese Score",
		dataIndex: "chinese",
	},
	{
		title: "Math Score",
		dataIndex: "math",
	},
	{
		title: "English Score",
		dataIndex: "english",
	},
];
const data = [
	{
		key: "1",
		name: "John Brown",
		chinese: 55,
		math: 60,
		english: 70,
	},
	{
		key: "2",
		name: "Jim Green",
		chinese: 98,
		math: 66,
		english: 89,
	},
	{
		key: "3",
		name: "Joe Black",
		chinese: 78,
		math: 90,
		english: 70,
	},
	{
		key: "4",
		name: "Jim Red",
		chinese: 88,
		math: 99,
		english: 89,
	},
];
```

先从把数据作为Props 灌入，组件写上接口，因为后面还需要做排序，或者渲染，所以2个字段做成可选，title部分有可能会传选择框之类的，所以类型写reactNode。

```typescript
export interface SourceDataType {
	key: string;
	[key: string]: any;
}
export interface ColumnType {
	title: ReactNode;
	/** 排序等操作用来代替这列的 */
	dataIndex: string;
	sorter?: {
		compare: (a: SourceDataType, b: SourceDataType) => number;
	};
	render?: (v: any, value: SourceDataType, rowData: ColumnType) => ReactNode;
}
export type TableProps = {
	/** 表内数据部分 */
	data: SourceDataType[];
	/** 表头部分*/
	columns: ColumnType[];
    /** 是否开启排序 */
	sorted?: boolean;
	/** 是否开启页码 */
	pagination?: boolean;
	/** 开启页码时才有效，设置每页显示几个*/
	pageSize?: number;
}

Table.defaultProps = {
	sorted: false,
	pagination: false,
	pageSize: 10
};
```

首先要将表格渲染出来：

```tsx
export function Table(props: TableProps) {
	const { data, columns, pageSize, pagination } = props;
	const [columnData, setColumnData] = useState<ColumnType[]>([]);
	const [sourceData, setSourceData] = useState<SourceDataType[]>([]);
	const [paginationData, setPaginationData] = useState<SourceDataType[][]>(
		[]
	);
	const [current, setCurrent] = useState(0); //这个是paginationData的索引
	const originPagination = useMemo(() => {
		return (data: SourceDataType[]) => {
			let tmp: SourceDataType[][] = [];
			let len = data.length;
			let pagenumber = Math.ceil(len / pageSize!); //页数
			for (let i = 0; i < pagenumber; i++) {
				//每页该显示多少内容做好。
				tmp[i] = data.slice(
					0 + i * pageSize!,
					pageSize! + i * pageSize!
				);
			}
			setPaginationData(tmp);
		};
	}, [pageSize]);
	const totalColumn = useMemo(() => {
		//表头总长
		setColumnData(columns); //表头拿来渲染
		return columns.length;
	}, [columns]);
	const totalLen = useMemo(() => {
		//内容部分总长
		setSourceData(data); //数据
		if (pagination) {
			//分页走paginationData
			originPagination(data);
		}
		return data.length;
	}, [data, originPagination, pagination]);
	const renderData = useMemo(() => {
		//内容部分渲染
		let render;
		if (pagination && paginationData.length !== 0) {
			//如果分页，渲染分页
			render = MapData(paginationData[current], columnData);
		} else {
			//否则直接渲染
			render = MapData(sourceData, columnData);
		}
		return render;
	}, [columnData, current, pagination, paginationData, sourceData]);

	return (
		<div>
			<table>
				<thead>
					<tr>
						{columnData.map((v, i) => {
							return (
								<th key={i}>
									<span>{v.title}</span>
								</th>
							);
						})}
					</tr>
				</thead>
				<tbody>{renderData}</tbody>
			</table>
			{pagination && (
				<Pagination
					style={{ justifyContent: "flex-end" }}
					total={totalLen}
					pageSize={pageSize}
					callback={(v) => setCurrent(v - 1)}
					defaultCurrent={1}
				></Pagination>
			)}
		</div>
	);
}
```

mapData的作用就是渲染数据，只要dataIndex对上了，那就渲染那个元素，比如选择框这里其实没有数据，但是表头有数据（配置的render），那么这个数据就会从表头的render处去拿。表头没有配的属性是不会显示的，所以比如checkbox可以去拿对应data的未在表头的那行数据做出对应的渲染。

```
const MapData = (data: SourceDataType[], columnData: ColumnType[]) => {
	return data.map((v) => {
		return (
			<tr key={v.key}>
				{columnData.map((value, index) => {
					return (
						<td key={index}>
							<span>
								{value.render
									? value.render(v[value.dataIndex], v, value)
									: v[value.dataIndex]}
							</span>
						</td>
					);
				})}
			</tr>
		);
	});
};
```

这样表格已经渲染出来了，但是表格有点丑，用styledcomponents加点样式：

```
const TableTable = styled.table`
	width: 100%;
	text-align: left;
	border-radius: 2px 2px 0 0;
	border-collapse: separate;
	border-spacing: 0;
	table-layout: auto;
	box-sizing: border-box;
	margin: 0;
	padding: 0;
	color: rgba(0, 0, 0, 0.65);
	font-variant: tabular-nums;
	line-height: 1.5715;
	list-style: none;
	font-feature-settings: "tnum";
	position: relative;
	z-index: 0;
	clear: both;
	font-size: 14px;
	background: #fff;
	border-radius: 2px;
	& > thead > tr > th {
		color: rgba(0, 0, 0, 0.85);
		font-weight: 500;
		text-align: left;
		background: #fafafa;
		border-bottom: 1px solid #f0f0f0;
		transition: background 0.3s ease;
		position: relative;
		padding: 16px;
		overflow-wrap: break-word;
	}
	& > tbody > tr {
		& > td {
			border-bottom: 1px solid #f0f0f0;
			transition: background 0.3s;
			position: relative;
			padding: 16px;
			overflow-wrap: break-word;
		}
		&:hover {
			& > td {
				background: #fafafa;
			}
		}
	}
`;
```

把table标签换成这个组件就可以了，这个样式我抄antd的，感觉用这个抄样式挺方便，喜欢啥样的可以自己改。

下面制作排序功能，排序功能主要也是靠表头配置。由于到时候需要恢复表格顺序，所以排序另起一个状态，同时还要记录第几列开启的排序：

```
	const [sortedData, setSortedData] = useState<SourceDataType[]>([]);
	const [filterState, setFilterState] = useState<number[]>([]);//记录第几列开启筛选
```

初始化时把记录第几列map给做了：

```
	const totalColumn = useMemo(() => {
		//表头总长
		setColumnData(columns); //表头拿来渲染
		setFilterState(new Array(columns.length).fill(0));//初始化排序数据
		return columns.length;
	}, [columns]);
```

渲染body判断部分加上对sortedData的判断：

```typescript
	const renderData = useMemo(() => {
		//内容部分渲染
		let render;
		if (pagination && paginationData.length !== 0) {
			//如果分页，渲染分页
			render = MapData(paginationData[current], columnData);
		} else {
			//否则直接渲染
			if (sortedData.length === 0) {
				render = MapData(sourceData, columnData);
			} else {//如果排序有数据，就按排序渲染
				render = MapData(sortedData, columnData);
			}
		}
		return render;
	}, [columnData, current, pagination, paginationData, sortedData, sourceData]);
```

在表头渲染时，进行判断，如果开启了sort并且配置compare，直接进行排序，拿到sortedData：

```tsx
							return (
								<th key={i}>
									<span>{v.title}</span>
									{v.sorter && sorted && v.sorter.compare && (
										<span
											onClick={() => {
												if (filterState[i]) {
													//如果已经开启了排序
													//查看是不是1，如果为1，进行逆序排序，否则清空
													if (filterState[i] === 1) {
														let res = sourceData
															.slice()
															.sort(
																(a, b) =>
																	-v.sorter!.compare(
																		a,
																		b
																	)
															); //数据传给compare
														let newfilter = new Array(
															totalColumn
														).fill(0);
														newfilter[i] = 2;
														setSortedData(res);
														setFilterState(
															newfilter
														);
													} else {
														setSortedData([]); //清空排序数据
														if (pagination) {
															originPagination(
																data
															);
														}
														filterState[i] = 0;
														setFilterState([
															...filterState,
														]);
													}
												} else {
													//没有开启就开启排序
													let res = sourceData
														.slice()
														.sort(
															v.sorter!.compare
														); //数据传给compare
													let newfilter = new Array(
														totalColumn
													).fill(0);
													newfilter[i] = 1;
													setSortedData(res);
													setFilterState(newfilter);
												}
											}}
										>
												<Icon
												icon="arrowup"
												block
												color={
													filterState[i] === 1
														? color.primary
														: color.dark
												}
											></Icon>
											<Icon
												icon="arrowdown"
												block
												color={
													filterState[i] === 2
														? color.primary
														: color.dark
												}
											></Icon>
										</span>
									)}
								</th>
							);
```

这样就完成了排序功能，但由于太丑，所以需要把这个svg美化一下：

```
const TableHeadSpan = styled.span`
	display: inline-block;
	position: absolute;
	right: 0;
	top: 0;
	padding: 16px;
	cursor: pointer;
	& svg {
		height: 10px;
		width: 10px;
	}
`;
```

把这个组件填充icon包裹的span就ok了。

这样表格组件就完成了，基本功能渲染+分页和排序。对于表格功能觉得不够的可以自行扩展，思路跟+sort思路一样。

下面制作story，story就简单写一下：

```tsx
const columns = [
	{
		title: "Name",
		dataIndex: "name",
	},
	{
		title: "Chinese Score",
		dataIndex: "chinese",
		sorter: {
			compare: (a: SourceDataType, b: SourceDataType) =>
				b.chinese - a.chinese,
		},
	},
	{
		title: "Math Score",
		dataIndex: "math",
		sorter: {
			compare: (a: SourceDataType, b: SourceDataType) => b.math - a.math,
		},
	},
	{
		title: "English Score",
		dataIndex: "english",
		sorter: {
			compare: (a: SourceDataType, b: SourceDataType) =>
				b.english - a.english,
		},
	},
];
const data = [
	{
		key: "1",
		name: "John Brown",
		chinese: 55,
		math: 60,
		english: 70,
	},
	{
		key: "2",
		name: "Jim Green",
		chinese: 98,
		math: 66,
		english: 89,
	},
	{
		key: "3",
		name: "Joe Black",
		chinese: 78,
		math: 90,
		english: 70,
	},
	{
		key: "4",
		name: "Jim Red",
		chinese: 88,
		math: 99,
		english: 89,
	},
];

export const knobsTable = () => (
	<Table
		columns={columns}
		data={data}
		sorted={boolean("sorted", true)}
		pagination={boolean("pagination", false)}
		pageSize={number("pageSize", 2)}
	></Table>
);

export const withPagination = () => (
	<Table columns={columns} data={data} pagination={true} pageSize={2}></Table>
);
```

## 今日作业

完成分页与表格组件

